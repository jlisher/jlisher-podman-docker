Name:           jlisher-podman-docker
Version:        0.1.0
Release:        1%{?dist}
Summary:        Podman compatibility as a docker-cli drop-in replacement
BuildArch:      noarch

License:        GPLv3+
URL:            https://gitlab.com/jlisher/%{name}

Source0:        https://sources.jlisher.com/%{name}/%{name}-%{version}.tar.gz
Source1:        https://sources.jlisher.com/%{name}/%{name}-%{version}.tar.gz.asc
Source2:        https://sources.jlisher.com/%{name}/%{name}-%{version}.tar.gz.sha256sum
Source3:        https://sources.jlisher.com/%{name}/%{name}-%{version}.tar.gz.sha256sum.asc
Source4:        https://sources.jlisher.com/gpgkey-17C57CB63CAF20BECB90B9CC4C930D5B79EBABCA.gpg
Source5:        https://sources.jlisher.com/gpgkey-17C57CB63CAF20BECB90B9CC4C930D5B79EBABCA.gpg.sha256sum

BuildRequires:  (gnupg2 and systemd-rpm-macros)
Requires:       (podman and systemd and (coreutils or coreutils-single))
Conflicts:      (podman-docker and moby-engine)

%description
This package helps provide further compatibility for using podman as a
docker-cli drop-in replacement.

%prep
# make sure everything runs successfully
set -euo pipefail

# Using `head -c 64` for checksums, as a sha256sum is 64 characters
# Checksums are not perfect, but doesn't hurt, too much

# Verify GPG keyring checksum (consistency)
[[ "$(sha256sum %{SOURCE4} | head -c 64)" == "$(head -c 64 %{SOURCE5})" ]] || {
  echo "Failed to verify the GPG keyring checksum"
  exti 1
}

# Verify tarball checksum GPG signature (authenticity)
%{gpgverify} --keyring='%{SOURCE4}' --signature='%{SOURCE3}' --data='%{SOURCE2}'

# Verify tarball checksum (consistency)
[[ "$(sha256sum %{SOURCE0} | head -c 64)" == "$(head -c 64 %{SOURCE2})" ]] || {
  echo "Failed to verify the tarball checksum"
  exti 1
}

# Verify tarball GPG signature (authenticity)
%{gpgverify} --keyring='%{SOURCE4}' --signature='%{SOURCE1}' --data='%{SOURCE0}'

# Run %setup
%setup -q

%install
# make sure everything runs successfully
set -euo pipefail

# create the required directory structure
mkdir -p %{buildroot}/%{_bindir} \
         %{buildroot}/%{_datadir}/bash-completion/completions \
         %{buildroot}/%{_unitdir} \
         %{buildroot}/%{_sysusersdir}

# install package files
install -Z -m 755 bin/docker \
        %{buildroot}/%{_bindir}/docker

install -Z -m 755 bash-completion/%{name} \
        %{buildroot}/%{_datadir}/bash-completion/completions/%{name}
ln -snr %{buildroot}/%{_datadir}/bash-completion/completions/%{name} \
        %{buildroot}/%{_datadir}/bash-completion/completions/docker

install -Z -m 644 systemd/%{name}.service \
        %{buildroot}/%{_unitdir}/%{name}.service
ln -snr %{buildroot}/%{_unitdir}/%{name}.service \
        %{buildroot}/%{_unitdir}/docker.service

install -Z -m 644 systemd/%{name}.socket \
        %{buildroot}/%{_unitdir}/%{name}.socket
ln -snr %{buildroot}/%{_unitdir}/%{name}.socket \
        %{buildroot}/%{_unitdir}/docker.socket

install -Z -m 644 %{name}.sysusers \
        %{buildroot}/%{_sysusersdir}/%{name}.conf

%pre
%sysusers_create_compat %{_sysusersdir}/%{name}.conf

%post
%systemd_post podman-docker.socket

%preun
%systemd_preun podman-docker.socket

%postun
%systemd_postun podman-docker.socket

%files
%{_bindir}/docker
%{_unitdir}/%{name}.service
%{_unitdir}/docker.service
%{_unitdir}/%{name}.socket
%{_unitdir}/docker.socket
%{_datadir}/bash-completion/completions/%{name}
%{_datadir}/bash-completion/completions/docker
%{_sysusersdir}/%{name}.conf
%license LICENSE
%doc README.md

%changelog
* Sat Apr 16 2022 Jarryd Lisher <jarryd@jlisher.com>
- Add GPG verification
* Fri Apr 15 2022 Jarryd Lisher <jarryd@jlisher.com>
- Fix user creation issue
* Tue Apr 12 2022 Jarryd Lisher <jarryd@jlisher.com>
- Add dockerroot user and group
- Enable podman-docker.socket by default
- Revert logging back to "info", staying inline with the default podman behaviour
* Fri Apr 08 2022 Jarryd Lisher <jarryd@jlisher.com>
- Initial release
