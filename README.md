# jlisher-podman-docker

This project helps provide further compatibility for using podman as a docker-cli drop-in replacement.

## Purpose

The focus of this project is to trick legacy applications and scripts, which only support rootfull docker containers, into behaving as if the docker daemon was running and listening on the unix socket `unix:///run/docker.sock`. It also ensures that
a `dockerroot` user and group is available and can be used to mimic the behaviour of rootfull docker allowing users of the `dockerroot` group access to the `unix:///run/docker.sock`.

This is primarily meant for development environments. I would highly recommend considering migrating your containers to either, systemd services (for small setups), or k8s pods (for larger deployments). k3s is also available for those looking for a
lightweight orchestrator.

## Installation

> This package is still in development and more testing is needed. However, please feel free to help with testing and provide feedback, it should be functional.

There is no "official" installation method, use whichever method best suits your needs.

- [RPM Repo](https://rpms.jlisher.com/) (coming soon)
- [RPM Package](https://rpms.jlisher.com/noarch/jlisher-podman-docker/)
- Manual install:
  - Download source files from:
    - [git repo](https://gitlab.com/jlisher/jlisher-podman-docker)
    - [source releases](https://sources.jlisher.com/jlisher-podman-docker/)
  - Unpack the archive (if needed)
  - `cd` into the directory
  - Run the installation steps (needs to be run as root):
    1. Create `dockerroot` user and group:
      - `getent group dockerroot 1>/dev/null 2>&1 || groupadd -r dockerroot`
      - `getent passwd dockerroot 1>/dev/null 2>&1 || useradd -r -g dockerroot -d /run/dockerroot -s /sbin/nologin -c "rootfull docker proxy user" dockerroot`
    2. Install the files:
      - `mkdir -p /usr/bin /etc/bash_completion.d /etc/systemd/system`
      - `install -Z -m 755 -t /usr/bin bin/docker`
      - `install -Z -m 644 -t /etc/bash_completion.d bash_completion.d/podman-docker`
      - `install -Z -m 644 -t /etc/systemd/system systemd/podman-docker.service systemd/podman-docker.socket`
    3. Activate the socket:
      - `systemctl daemon-reload`

### Activate Socket

You will need to activate the systemd socket to use this package:

```shell
systemctl enable --now podman-docker.socket
```

> We control the service through the socket, rather than using the `.service` unit, as this ensure the behaviour we expect.
> For example, starting `podman-docker.service` will activate `podman-docker.socket`, however, stopping `podman-docker.service` will not deactivate `podman-docker.socket`. This leaves the system in a state that is unintended from the administrator's expectations.
>
> This is a result of using a socket based service architecture (It probably has a real name, but that's not important), as the socket will always ensure the service is running.

## Technical Information

Currently, there are 3 components:

- [docker cli script](./bin/docker): uses `exec` to run `podman` using `--remote` and `--url` options.
- [docker cli completions](./bash_completion.d/podman-docker): Bash completions using the podman completions.
- [systemd service and socket](./systemd)
  - [systemd service](./systemd/podman-docker.service): A namespaced service for the Podman API.
  - [systemd socket](./systemd/podman-docker.socket): A socket at `[/var]/run/docker.sock`, owned by `dockerroot`, which is used to access the service.

### Running `docker`

In trying to keep a familiar interface for the docker only integrations, a daemon service `podman-docker.service` is made available via a unix socket `unix:///run/docker.sock`, which is created by the `podman-docker.socket` systemd socket.

This socket should be enabled and created during the installation, however, if you have any issues you can simply control the socket using the `systemctl` command.

You can also monitor any error from the service, simply query the `podman-docker.service` service. By default, the log level is set to `warn`, which does differ from podman's default behaviour.

### Running As Non-root User (rootfull containers)

> Please read the [security concerns from Docker](https://docs.docker.com/engine/install/linux-postinstall/#manage-docker-as-a-non-root-user)
>
> These can be mitigated, or rather lessened, by utilising [udica](https://github.com/containers/udica) to generate (or create your own) selinux policies, and using `subuid` and `subgid` mappings to prevent containers from gaining privileged access.
>
> A container should never be given un-auditable access to the host, regardless of the effective uid. ACLs should also be monitored and frequently audited, to ensure no malicious (or unexpected) activity is accruing on your systems.
>
> Modern security has multiple levels, it's about finding the correct combination of those levels for your needs.

There is a `dockerroot` user and group available, which acts as a proxy user to access rootfull containers, using the socket.

Simply add your user to the `dockerroot` group:

```shell
sudo usermod -aG dockerroot $USER
```

> The `/usr` directory is available as read-only to the `podman-docker.service`.
>
> This is a security measure, however, it isn't perfect or even a very good measure, but it helps prevent containers from altering the host's system files. So, better than nothing.

## Development

This project uses the [jlisher-package-template](https://gitlab.com/jlisher/jlisher-package-template) (more documentation is needed, will get to that). However, it provides a few simple options to adjust the basic packaging and publishing options, though
it may seem like double work, so probably will to change soon.

As there is no code to compile in this project, there is no build phase. We simply have to package the source files (structured for simpler development) in an installable layout, then publish our package.

### File Based Options

The following files provide a way to adjust our packaging and publishing variables. These files can be committed to a VCS, allowing an architecture or OS (which has special requirements) to maintain an independent branch in an obvious way. These values
should correspond with the values set in [jlisher-podman-docker.spec](./jlisher-podman-docker.spec). At the moment, you need to manually set the values.

- `.name`:
  - The name to use for the package
  - Usage:
    - Used by all scripts in [./scripts](./scripts)
    - To create and determine the file name and path of:
      - The Source tarball
      - The RPM `.spec` file
      - The RPM package file
      - The SRPM package file
      - The remote directory for the source and package publishing
- `.version`
  - The current version of the package
  - Usage:
    - Used by all scripts in [./scripts](./scripts)
    - To create and determine the file name and path of:
      - The Source tarball
      - The RPM package file
      - The SRPM package file
- `.arch`
  - The current architecture we are packaging for
  - Usage:
    - Used by [./scripts/publish-rpms.sh](./scripts/publish-rpms.sh)
    - To determine the file name and path of:
      - The RPM package file
      - The SRPM package file
- `.build`
  - The distribution release version
  - Usage:
    - Used by [./scripts/publish-rpms.sh](./scripts/publish-rpms.sh)
    - To determine the file name and path of:
      - The RPM package file

### Packaging

The following is some instructions on how you can go about preparing to package the source. You could always use the [srpm releases](https://rpms.jlisher.com/src/jlisher-podman-docker/)
or [source releases](https://sources.jlisher.com/jlisher-podman-docker/), if you don't want the extra publishing scripts (see [Publishing](#Publishing) below).

#### Packaging Dependencies:

- rpm-build
- tar
- gzip
- realpath

Please review [build-source-tar.sh](./scripts/build-source-tar.sh), [build-rpm.sh](./scripts/build-rpm.sh), and [jlisher-podman-docker.spec](./jlisher-podman-docker.spec) for information on building.

### Publishing

Most people probably won't care too much about the publishing scripts, provided in the [jlisher-podman-docker git repo](https://gitlab.com/jlisher/jlisher-podman-docker). They likely don't fit most use cases, but it is simple enough for my needs.

#### Publishing Dependencies:

- [sign-and-sum](https://gitlab.com/jlisher/sign-and-sum)
- openssh-clients

Please review [publish-source.sh](./scripts/publish-source.sh) and [publish-rpms.sh](./scripts/publish-rpms.sh) for more information.
